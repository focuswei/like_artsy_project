//
//  LGTreeNode.h
//  LGLikeArtsyProject
//
//  Created by Ken on 2022/5/19.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface LGTreeNode : NSObject

/**
 值
 */
@property (nonatomic, assign) NSInteger value;
/**
 左节点
 */
@property (nonatomic, strong) LGTreeNode *leftNode;
/**
 右节点
 */
@property (nonatomic, strong) LGTreeNode *rightNode;

+ (LGTreeNode *)treeNodeAtIndex:(NSInteger)index inTree:(LGTreeNode *)rootNode;

+ (LGTreeNode *)addTreeNode:(LGTreeNode *)node value:(NSInteger)value;

- (void)preOrderTraverseTree:(LGTreeNode *)rootNode handler:(void(^)(LGTreeNode *node))handler;

- (void)inOrderTraverseTree:(LGTreeNode *)rootNode handler:(void(^)(LGTreeNode *))handle;

- (void)postOrderTraverseTree:(LGTreeNode *)rootNode handler:(void(^)(LGTreeNode *))handler;

- (void)levelTraverseTree:(LGTreeNode *)rootNode handler:(void(^)(LGTreeNode *))handler;

+ (NSInteger)depthOfTree:(LGTreeNode *)rootNode;

@end

NS_ASSUME_NONNULL_END
