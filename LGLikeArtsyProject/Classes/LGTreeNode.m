//
//  LGTreeNode.m
//  LGLikeArtsyProject
//
//  Created by Ken on 2022/5/19.
//

#import "LGTreeNode.h"

@implementation LGTreeNode

+ (LGTreeNode *)creatTreeWithValues:(NSArray *)values {
    //是不是应该对values进行大小排序
    LGTreeNode *root = nil;
    for (int i = 0; i < values.count; i++) {
        //int 4个字节 NSInteger 8个字节
        int value = [[values objectAtIndex:i] intValue];
        root = [LGTreeNode addTreeNode:root value:value];
    }
    return root;
}

/**
 递归函数 增加节点

 @param node 新节点
 @param value 节点的值
 @return 返回一个树
 */
+ (LGTreeNode *)addTreeNode:(LGTreeNode *)node value:(NSInteger)value {
    if (!node) {
        //新的值
        node = [LGTreeNode new];
        node.value = value;
    } else if (value <= node.value) {
        //先插入，左节点
        node.leftNode = [LGTreeNode addTreeNode:node.leftNode value:value];
    } else {
        //插入，右节点
        node.rightNode = [LGTreeNode addTreeNode:node.rightNode value:value];
    }
    return node;
}


/**
 遍历根节点,找到位置
 @param rootNode 根节点
 @return 返回位置的节点
 */
+ (LGTreeNode *)treeNodeAtIndex:(NSInteger)index inTree:(LGTreeNode *)rootNode {
    
    if (rootNode || index<0) {
        return nil;
    }
    
    NSMutableArray *queueArr = [NSMutableArray array];
    [queueArr addObject:rootNode];
    //非递归
    while (queueArr.count > 0) {
        //根节点
        LGTreeNode *node = [queueArr firstObject];
        if (index == 0) {
            //到达目标节点
            return node;
        }
        //先进先出
        [queueArr removeObjectAtIndex:0];
        index--; //移除节点，index减少
        
        if (node.leftNode) {
            //假如左边有，进入左边节点
            [queueArr addObject:node.leftNode];
        }
        if (node.rightNode) {
            //假如右边也有，进入右边节点
            [queueArr addObject:node.rightNode];
        }
    }
    
    //层次遍历完，仍然没有找到位置，返回nil
    return nil;
}


/**
 先序遍历当前节点树
   1
  246
 357
 @param rootNode 当前节点
 @param handler 访问节点处理函数
 */
- (void)preOrderTraverseTree:(LGTreeNode *)rootNode handler:(void(^)(LGTreeNode *node))handler {
    
    if (rootNode) {
        
        if (handler) {
            //返回根节点
            handler(rootNode);
        }
        
        [self preOrderTraverseTree:rootNode.leftNode handler:handler];
        [self preOrderTraverseTree:rootNode.rightNode handler:handler];
    }
}

/**
 中序遍历
 先遍历左子树，再访问根
    4
   3 6
  2 5 8
 1   7  9
 @param rootNode 根节点
 @param handler
 */
- (void)inOrderTraverseTree:(LGTreeNode *)rootNode handler:(void(^)(LGTreeNode *))handler {
    
    if (rootNode) {
        //开启左子树递归
        [self inOrderTraverseTree:rootNode handler:handler];
        if (handler) {
            //返回根节点
            handler(rootNode);
        }
        //开启右子树递归
        [self inOrderTraverseTree:rootNode handler:handler];
    }
}



/**
 后序遍历
 先遍历左子树，再遍历右子树，再访问根
     10 11
   6 7 8 9
 1  2  3 4 5
 @param rootNode 根节点
 @param handler
 */
- (void)postOrderTraverseTree:(LGTreeNode *)rootNode handler:(void(^)(LGTreeNode *))handler {
    
    if (rootNode) {
        //开启左子树递归
        [self postOrderTraverseTree:rootNode.leftNode handler:handler];
        
        //开启右子树递归
        [self postOrderTraverseTree:rootNode.rightNode handler:handler];
        if (handler) {
            //返回右节点
            handler(rootNode);
        }
    }
}

/**
 层次遍历（广度优先）
 先遍历完一层，再遍历下一层，因此又叫广度优先遍历
 @param rootNode 二叉树根节点
 @param handler
 */
- (void)levelTraverseTree:(LGTreeNode *)rootNode handler:(void(^)(LGTreeNode *))handler {
    if (!rootNode) {
        return;
    }
    
    NSMutableArray *queueMuArr = [NSMutableArray array];
    [queueMuArr addObject:rootNode];
    while (queueMuArr.count > 0) {
        LGTreeNode *node = queueMuArr[0];
        
        if (handler) {
            handler(node);
        }
        
        [queueMuArr removeObject:node];
        if (node.leftNode) {
            [queueMuArr addObject:node.leftNode]; //压入左节点
        }
        if (node.rightNode) {
            [queueMuArr addObject:node.rightNode]; //压入右节点
        }
    }
    
}


/**
 二叉树的深度
 1.如果根节点为空，深度为0；
 2.如果左右节点为空，深度为1；
 3.递归：二叉树的深度，取左右节点较深的一个+1；
 @param rootNode 根节点
 @return 二叉树深度
 */
+ (NSInteger)depthOfTree:(LGTreeNode *)rootNode {
    if (!rootNode) {
        return 0;
    }
    if (!rootNode.leftNode && !rootNode.rightNode) {
        return 1;
    }
    
    //左子树深度
    NSInteger leftDepth = [self depthOfTree:rootNode.leftNode];
    //右子树深度
    NSInteger rightDepth = [self depthOfTree:rootNode.rightNode];
    
    return MAX(leftDepth, rightDepth) + 1;
}

/**
 二叉树的宽度

 @param rootNode 二叉树的节点
 @return 宽度
 */
+ (NSInteger)widthOfTree:(LGTreeNode *)rootNode {
    if (!rootNode) {
        return 0;
    }
    
    NSMutableArray *queueArr = [NSMutableArray array];
    [queueArr addObject:rootNode];
    NSInteger maxWidth = 1; //最大的宽度，初始化为1
    NSInteger curWidth = 0; // 当前层数的宽度
    
    while (queueArr.count > 0) {
        
        curWidth = queueArr.count;
        for (int i = 0; i < curWidth; i++) {
            LGTreeNode *node = [queueArr firstObject];
            [queueArr removeObjectAtIndex:0];
            
            if (node.leftNode) {
                [queueArr addObject:node.leftNode];
            }
            
            if (node.rightNode) {
                [queueArr addObject:node.rightNode];
            }
        }
        //宽度：当前层节点树
        maxWidth = MAX(maxWidth, queueArr.count);
    }
    return maxWidth;
}

/**
 二叉树的所有节点数

 @param rootNode 根节点
 @return 节点数
 */
+ (NSInteger)numberOfNodesInTree:(LGTreeNode *)rootNode {
    if (!rootNode) {
        return 0;
    }
    return [self numberOfNodesInTree:rootNode.leftNode] + [self numberOfNodesInTree:rootNode.rightNode] + 1;
}


/**
 二叉树某层中的节点数

 @param level 层
 @param rootNode 节点
 @return
 */
+ (NSInteger)numberOfNodesOnLevel:(NSInteger)level inTree:(LGTreeNode *)rootNode {
    if (!rootNode || level < 1) {
        return 0;
    }
    if (level == 1) {
        return 1;
    }
    //递归：level层节点数 = 左子树level-1层节点数+右子树level-1层节点数
    return [self numberOfNodesOnLevel:level-1 inTree:rootNode.leftNode] + [self numberOfNodesOnLevel:level-1 inTree:rootNode.rightNode];
}

/**
 二叉树叶子节点数

 @param rootNode 根节点
 @return 叶子节点数
 */
+ (NSInteger)numberOfLeafsInTree:(LGTreeNode *)rootNode {
    
    if (!rootNode) {
        return 0;
    }
    //左子树和右子树都是空，说明是叶子
    if (!rootNode.rightNode && !rootNode.leftNode) {
        
        return 1;
    }
    return [self numberOfLeafsInTree:rootNode.leftNode] + [self numberOfLeafsInTree:rootNode.rightNode];
}


/**
 二叉树最大距离

 @param rootNode 根节点
 @return
 */
+ (NSInteger)maxDistanceOfTree:(LGTreeNode *)rootNode {
    if (!rootNode) {
        return 0;
    }
    //方案一:(递归次数较多，效率较低)
    //1.最远距离经过根节点：距离h = 左子树深度 + 右子树深度
    NSInteger distance = [self depthOfTree:rootNode.leftNode] + [self depthOfTree:rootNode.rightNode];
    //2.最远距离在根节点左子树上，即计算左叶子树最远距离；
    NSInteger disLeft = [self maxDistanceOfTree:rootNode.leftNode];
    //3.最远距离在右子树上
    NSInteger disRight = [self maxDistanceOfTree:rootNode.rightNode];
    return MAX(MAX(disLeft, disRight), distance);
    
}

/**
 
 *  计算树节点的最大深度和最大距离
 *
 *  @param rootNode 根节点
 *
 *  @return TreeNodeProperty
 
 + (TreeNodeProperty *)propertyOfTreeNode:(BinaryTreeNode *)rootNode {
 
 if (!rootNode) {
 return nil;
 }
 
 TreeNodeProperty *left = [self propertyOfTreeNode:rootNode.leftNode];
 TreeNodeProperty *right = [self propertyOfTreeNode:rootNode.rightNode];
 TreeNodeProperty *p = [TreeNodeProperty new];
 //节点的深度depth = 左子树深度、右子树深度中最大值+1（+1是因为根节点占了1个depth）
 p.depth = MAX(left.depth, right.depth) + 1;
 //最远距离 = 左子树最远距离、右子树最远距离和横跨左右子树最远距离中最大值
 p.distance = MAX(MAX(left.distance, right.distance), left.depth+right.depth);
 
 return p;
 }
 */


/**
 二叉树某一个节点到根节点的路径

 @param treeNode 节点
 @param rootNode 根节点
 @return
 */
+ (NSArray *)pathOfTreeNode:(LGTreeNode *)treeNode inTree:(LGTreeNode *)rootNode {
    NSMutableArray *pathArray = [NSMutableArray array];
    [self isFoundTreeNode:treeNode inTree:rootNode routePath:pathArray];
    return pathArray;
    
}

/**
    查找某个节点是否在树中

    @param treeNode 待查找的节点
    @param rootNode 根节点
    @param path  根节点到待查找节点的路径
    @return YES：找到，NO：未找到
 */
+ (BOOL)isFoundTreeNode:(LGTreeNode *)treeNode inTree:(LGTreeNode *)rootNode routePath:(NSMutableArray *)path {
    
    if (!rootNode || !treeNode) {
        return NO;
    }
    
    //找到节点
    if (rootNode == treeNode) {
        [path addObject:rootNode];
        return YES;
    }
    //压入根节点，进行递归
    [path addObject:rootNode];
    //先入左子树中查找
    BOOL find = [self isFoundTreeNode:treeNode inTree:rootNode.leftNode routePath:path];
    if (!find) {
        find = [self isFoundTreeNode:treeNode inTree:rootNode.rightNode routePath:path];
    }
    //如果两边都没找到，则弹出根节点
    if (!find) {
        [path removeLastObject];
    }
    return find;
}

/**
 二叉树中两个节点最近的公共父节点

 @param nodeA 第一个节点
 @param nodeB 第二个节点
 @param rootNode 二叉树根节点
 @return
 */
+ (LGTreeNode *)parentOfNode:(LGTreeNode *)nodeA andNode:(LGTreeNode *)nodeB inTree:(LGTreeNode *)rootNode {
    if (!rootNode || !nodeA || !nodeB) {
        return nil;
    }
    if (nodeA == nodeB) {
        return nodeA;
    }
    //从根节点到节点A的路径
    NSArray *pathA = [self pathOfTreeNode:nodeA inTree:rootNode];
    NSArray *pathB = [self pathOfTreeNode:nodeB inTree:rootNode];
    //其中一个节点不在树中，则没有公共父节点
    if (pathA.count == 0 || pathB.count == 0){
        return nil;
    }
    
    //从后往前推，查找第一个出现的公共节点
    for (NSInteger i = pathA.count - 1; i >= 0; i--) {
        for (NSInteger j = pathB.count - 1; j >= 0; j--) {
            if ([pathA objectAtIndex:i] == [pathB objectAtIndex:j]) {
                return [pathA objectAtIndex:i];
            }
        }
    }
    
    return nil;
}

/**
 二叉树中两个节点之间的路径

 @param nodeA 第一个节点
 @param nodeB 第二个节点
 @param rootNode 根节点
 @return
 */
+ (NSArray *)pathFromNode:(LGTreeNode *)nodeA toNode:(LGTreeNode *)nodeB inTree:(LGTreeNode *)rootNode {
    if (!rootNode || !nodeA || !nodeB) {
        return nil;
    }
    NSMutableArray *path = [NSMutableArray array];
    if (nodeA == nodeB) {
        [path addObject:nodeA];
        [path addObject:nodeB];
        return path;
    }
    //从根节点到节点A的路径
    NSArray *pathA = [self pathOfTreeNode:nodeA inTree:rootNode];
    NSArray *pathB = [self pathOfTreeNode:nodeB inTree:rootNode];
    if (pathA.count == 0 || pathB.count == 0){
        return nil;
    }
    
    for (NSInteger i = pathA.count - 1; i >= 0; i--) {
        [path addObject:[path objectAtIndex:i]];
        for (NSInteger j = pathB.count - 1; j >= 0; j--) {
            if ([pathA objectAtIndex:i] == [pathB objectAtIndex:j]) {
                j++;
                while (j<pathB.count) {
                    [path addObject:[pathB objectAtIndex:j]];
                    j++;
                }

                return path;
            }
        }
    }
    return nil;
}

/**
 二叉树两个节点之间的距离

 @param nodeA 第一个节点
 @param nodeB 第二个节点
 @param rootNode 根节点
 @return 两个节点的距离没有返回-1
 */
+ (NSInteger)distanceFromNode:(LGTreeNode *)nodeA toNodeL:(LGTreeNode *)nodeB inTree:(LGTreeNode *)rootNode {
    if (!rootNode || !nodeA || !nodeB) {
        return -1;
    }
    if (nodeA == nodeB) {
        return 0;
    }
    //从根节点到节点A的路径
    NSArray *pathA = [self pathOfTreeNode:nodeA inTree:rootNode];
    NSArray *pathB = [self pathOfTreeNode:nodeB inTree:rootNode];
    if (pathA.count == 0 || pathB.count == 0){
        return -1;
    }
    
    //i 是左节点的深度
    for (NSInteger i = pathA.count - 1; i >= 0; i--) {
        for (NSInteger j = pathB.count - 1; j >= 0; j--) {
            if ([pathA objectAtIndex:i] == [pathB objectAtIndex:j]) {
                //找到公共节点
                return pathA.count - i + pathB.count - j -2;
            }
        }
    }
    return -1;
}

- (LGTreeNode *)invertTreeNode:(LGTreeNode *)rootNode {
    if (!rootNode) {
        return nil;
    }
    if (!rootNode.leftNode && !rootNode.rightNode) {
        return rootNode;
    }
    
    LGTreeNode *temp = rootNode.leftNode;
    rootNode.leftNode = rootNode.rightNode;
    rootNode.rightNode = temp;
    
    if (rootNode.leftNode) {
        [self invertTreeNode:rootNode.leftNode];
    }
    if (rootNode.rightNode) {
        [self invertTreeNode:rootNode.rightNode];
    }
    return rootNode;
}

@end
