//
//  LGStringHelper.h
//  LGLikeArtsyProject
//
//  Created by Ken on 2022/5/19.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LGStringHelper : NSObject

//字符串文字的长度
+ (CGFloat)widthOfString:(NSString *)string font:(UIFont*)font height:(CGFloat)height;

//字符串文字的高度
+ (CGFloat)heightOfString:(NSString *)string font:(UIFont*)font width:(CGFloat)width;


@end

NS_ASSUME_NONNULL_END
