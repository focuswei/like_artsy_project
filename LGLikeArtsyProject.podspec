

Pod::Spec.new do |s|
  s.name             = 'LGLikeArtsyProject'
  s.version          = '0.1.4'
  s.summary          = 'This is ObjC Framework , name is LGLikeArtsyProject.'


  s.homepage         = 'https://gitlab.com/focuswei/like_artsy_project'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'focuswei' => 'w394966935@gmail.com' }
  s.source           = { :git => 'https://gitlab.com/focuswei/like_artsy_project.git', :tag => s.version }

  s.platform         = :ios, "12.0"
  
  s.source_files     = 'LGLikeArtsyProject/Classes/**/*'
  s.framework        = 'UIKit'
  #s.vendored_frameworks = 'Build/LGLikeArtsyProject.framework'
  
end
