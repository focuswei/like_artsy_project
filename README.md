--embedded# LGLikeArtsyProject

[![CI Status](https://img.shields.io/travis/focuswei/LGLikeArtsyProject.svg?style=flat)](https://travis-ci.org/focuswei/LGLikeArtsyProject)
[![Version](https://img.shields.io/cocoapods/v/LGLikeArtsyProject.svg?style=flat)](https://cocoapods.org/pods/LGLikeArtsyProject)
[![License](https://img.shields.io/cocoapods/l/LGLikeArtsyProject.svg?style=flat)](https://cocoapods.org/pods/LGLikeArtsyProject)
[![Platform](https://img.shields.io/cocoapods/p/LGLikeArtsyProject.svg?style=flat)](https://cocoapods.org/pods/LGLikeArtsyProject)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

LGLikeArtsyProject is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'LGLikeArtsyProject'
```

## Author

focuswei, w394966935@gmail.com

## License

LGLikeArtsyProject is available under the MIT license. See the LICENSE file for more info.
