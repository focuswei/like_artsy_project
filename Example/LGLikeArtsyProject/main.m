//
//  main.m
//  LGLikeArtsyProject
//
//  Created by focuswei on 05/19/2022.
//  Copyright (c) 2022 focuswei. All rights reserved.
//

@import UIKit;
#import "LGAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([LGAppDelegate class]));
    }
}
