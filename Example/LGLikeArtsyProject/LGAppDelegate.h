//
//  LGAppDelegate.h
//  LGLikeArtsyProject
//
//  Created by focuswei on 05/19/2022.
//  Copyright (c) 2022 focuswei. All rights reserved.
//

@import UIKit;

@interface LGAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
